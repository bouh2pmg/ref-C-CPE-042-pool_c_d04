#include <unistd.h>

void	my_putstr(char *str)
{
  int	i;

  i = -1;
  while (str[++i])
    write(1, &(str[i]), 1);
}
