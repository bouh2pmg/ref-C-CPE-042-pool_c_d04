/* my_getnbr.c for my_getnbr in /home/jean_j/rendu/Piscine_C_J04
** 
** Made by jean jonathan
** Login   <jean_j@epitech.net>
** 
** Started on  Thu Oct  1 16:18:06 2015 jean jonathan
** Last update Thu Oct  1 19:00:07 2015 jean jonathan
*/

int is_neg(char *str)
{
  int count;
  int i;
  i = 0;
  count = 0;
  while (str[i] < '0' || str[i] > '9')
    {
      if (str[i] == '-')
	count++;
      i++;
    }
  if (count % 2 != 0)
    return (1);
  else
    return (0);
}

int is_base(char a)
{
  if (a >= '0' && a <= '9')
    {
      return (1);
    }
  else
    {
      return (0);
    }
}

int my_getnbr(char *str)
{
  int i;
  long long output;
  int neg;
  int end;
  i = 0;
  output = 0;
  neg = 0;
  while (is_base(str[i]) == 0)
    i++;
  while (is_base(str[i]) == 1 && end != 1)
    {
      output = (output * 10) + (str[i] - 48);
      if (output > 2147483648)
	{
	  output = 0;
	  end = 1;
	}
      i++;
    }
  if (is_neg(str) == 1)
    output = output * (-1);
  else if (output == 2147483648)
    output = 0;
  return (output);
}
