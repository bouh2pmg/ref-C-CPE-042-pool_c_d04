#include <unistd.h>

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    ++i;
  return (i);
}

int	my_putstr(char *str)
{
  return (write(1, str, my_strlen(str)));
}

int	my_strcmp(char *s1, char *s2)
{
  int	size1 = my_strlen(s1);
  int	size2 = my_strlen(s2);
  int	i = 0;
  
  if (size1 != size2)
    return (size2 - size1);
  size1 = size2 = 0;
  while (s1[i] && s2[i])
    {
      size1 += s1[i];
      size2 += s2[i];
      ++i;
    }
  return (size1 - size2);
}

void	my_swap(char **s1, char **s2)
{
  char	*tmp;
  
  tmp = *s1;
  *s1 = *s2;
  *s2 = tmp;
}

char	**my_sort_array(char **tab, int size)
{
  int	i = size - 1;
  int	j;
  
  while (i > 0)
    {
      j = 0;
      while (j <= i - 1)
	{
	  if (my_strcmp(tab[j], tab[j + 1]) < 0)
	    {
	      my_swap(&tab[j], &tab[j + 1]);
	    }
	  ++j;
	}
      --i;
    }
  return (tab);
}
 
void	my_show_array(char **tab, int size)
{
  int	i;
  
  tab = my_sort_array(tab, size);
  i = 0;
  while (i < size)
    {
      my_putstr(tab[i]);
      my_putstr("\n");
      ++i;
    }
}
